FROM openjdk:8
ADD ./target/calculedistanceville-0.0.1-SNAPSHOT.jar /ville_calcule_distance/calculedistanceville.jar
EXPOSE 8086
ENTRYPOINT ["java","jar","/ville_calcule_distance/calculedistanceville.jar"]
