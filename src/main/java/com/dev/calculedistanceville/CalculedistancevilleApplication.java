package com.dev.calculedistanceville;

import com.dev.calculedistanceville.model.Ville;
import com.dev.calculedistanceville.repository.VilleRepository;
import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CalculedistancevilleApplication implements CommandLineRunner {


    private VilleRepository villeRepository;

    public static void main(String[] args) {

        SpringApplication.run(CalculedistancevilleApplication.class, args);


    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Override
    public void run(String... args) throws Exception {

        villeRepository.save(Ville.builder()
                .nom("Alger")
                .pays("Algérie")
                .codePostal("16000")
                .latitude(36.752887)
                .longitude(3.042048)
                .build());

        villeRepository.save(Ville.builder()
                .nom("Annaba")
                .pays("Algérie")
                .codePostal("23000")
                .latitude(36.8982165)
                .longitude(7.7549272)
                .build());

        villeRepository.save(Ville.builder()
                .nom("Oran")
                .pays("Algérie")
                .codePostal("31020")
                .latitude(35.72731926855027)
                .longitude(-0.6576117283151106)
                .build());

        villeRepository.save(Ville.builder()
                .nom("Constantine")
                .pays("Algérie")
                .codePostal("25000")
                .latitude(36.364519)
                .longitude(6.60826)
                .build());
    }
}
