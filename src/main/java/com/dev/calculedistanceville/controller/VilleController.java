package com.dev.calculedistanceville.controller;

import com.dev.calculedistanceville.DTO.VilleDTO;
import com.dev.calculedistanceville.service.VilleService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * The VilleController Class
 * Handiling HTTP requests
 *
 * @author BADREDDINE BENBAZIZ
 * @version 1.0
 * Date 29/07/2022.
 */

@RestController
@RequestMapping("/api/v1/villes")
@AllArgsConstructor
public class VilleController {


    // Le service VilleService

    private final VilleService villeService;

    @GetMapping(path = "{villeId}")
    public ResponseEntity<VilleDTO> getVille(@PathVariable("villeId") Integer villeId) {

        if (villeService.findById(villeId).isPresent())
            // si la ville existe dans la base de données
            return new ResponseEntity<VilleDTO>(villeService.findById(villeId).get(), HttpStatus.OK);

        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<VilleDTO>> getAllVilles() {

        List<VilleDTO> villeDTOList = villeService.findAll();
        if (villeDTOList.size() > 0)
            return new ResponseEntity<List<VilleDTO>>(villeService.findAll(), HttpStatus.OK);
        else return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}
