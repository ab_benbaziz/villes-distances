package com.dev.calculedistanceville.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "T_VILLE")
public class Ville {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ville", unique = true, nullable = false)
    private long id;

    @Column(name = "NOM_VILLE")
    private String nom;


    @Column(name = "ZIPCODE")
    private String codePostal;


    @Column(name = "PAYS_VILLE")
    private String pays;

    @Column(name = "LATITUDE_VILLE  ", nullable = false)
    private Double latitude;

    @Column(name = "LONGITUDE_VILLE", nullable = false)
    private Double longitude;
}
