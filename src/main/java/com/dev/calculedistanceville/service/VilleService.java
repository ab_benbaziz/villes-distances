package com.dev.calculedistanceville.service;

import com.dev.calculedistanceville.DTO.VilleDTO;
import com.dev.calculedistanceville.model.Ville;
import com.dev.calculedistanceville.repository.VilleRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class VilleService {

    /* Repoository*/

    private final VilleRepository villeRepository;

    private final ModelMapper modelMapper;


    public VilleDTO save(VilleDTO ville) {


        return modelMapper.map(villeRepository.save(modelMapper.map(ville, Ville.class)), VilleDTO.class);
    }


    public Boolean delete(long id) {
        if (villeRepository.existsById(id)) {
            villeRepository.deleteById(id);
            return true;
        }
        return false;

    }


    public VilleDTO update(VilleDTO ville) {

        return modelMapper.map(villeRepository.save(modelMapper.map(ville, Ville.class)), VilleDTO.class);
    }

    /*Cette method retourne
            une ville si l'enregistrement existe, et
            null si l'enregistrement n'existe pas.
     */
    public Optional<VilleDTO> findById(long id) {
        if (villeRepository.findById(id).isPresent()) {
            Ville ville = villeRepository.findById(id).get();
            VilleDTO villeDTO = modelMapper.map(ville, VilleDTO.class);
            return Optional.of(villeDTO);
        } else return Optional.empty();
    }


    public List<VilleDTO> findAll() {
        //Récupérre la liste des villes
        List<VilleDTO> villeDTOList = villeRepository.findAll()
                .stream()
                .map(ville -> modelMapper.map(ville, VilleDTO.class)) //mapping dans villeDTO
                .collect(Collectors.toList());

        return villeDTOList;
    }


}
